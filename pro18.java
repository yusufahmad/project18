import java.util.Scanner;
public class Assignment18 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        // This prog accept 2 integer num & which 1 is bigger
        Scanner input = new Scanner (System.in);
        System.out.println("Enter the first and second integer number: ");
        int num1 = input.nextInt();
         int num2 = input.nextInt();
         String result;
          result = (num1 > num2)? (num1 +" is greater than " + num2): (num2 +" is greater than " + num1);
          System.out.println(result);
        
    }
    
}
